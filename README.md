# Face analyzer

Analyzer face App


## Installation

You need install python 3.9:

```
pip install python
```

## Important import
```
from deepface import DeepFace

```

## Getting started

App strat:

```
python face.py
```

## Technologies used

- Python
- JetBrains
- JSON


## Description

App has 2 functions:
```
def face_verify(img_1, img_2):
```
This function allows you to compare two photos of a person and indicate if this is the same person.
```
def face_analyze():
```
This function allows to indicate a person. In my case actions=['age','gender','race','emotion']

I save as JSON file the result of both functions.


## Author

This app was done by Dmitry Tsunaev.

- [linkedin](http://linkedin.com/in/dmitry-tsunaev-530006aa)

